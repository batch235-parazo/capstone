const chai = require('chai');
const{ assert }= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	

	it('test_api_post_currency_returns_200_if_complete_input_given', () => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
	})

})



describe('Currency Test Suite', () => {

	it('test post currency is running', (done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test post currency returns 400 if name is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if name is not a string',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			name: 123456789,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if name is an empty string',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			name: "",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if ex is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			name: "Sample Name"
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if ex is not an object',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			name: "Sample Name",
			ex: 'usd'
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if ex is an empty object',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			name: "Sample Name",
			ex: {}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if alias is missing',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			name: "Sample Name",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if alias is not a string',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 123456789,
			name: "Sample Name",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if alias is an empty string',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "",
			name: "Sample Name",
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 400 if all fields are complete but there is a duplicate alias',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: "usd",
			name: "United States Dollar",
			ex: {
				'peso': 50.73,
				'won': 1187.24,
				'yen': 108.63,
				'yuan': 7.03
			}	
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			done();
		})
	})

	it('test post currency returns 200 if all fields are complete and there are no duplicates',(done) => {
		chai.request('http://localhost:5001')
		.post('/forex/currency')
		.type('json')
		.send({
			alias: 'Sample',
			name: 'Sample Name',
			ex: {
				'peso': 1,
		        'usd': 2,
		        'won': 3,
		        'yuan': 4
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			done();
		})
	})

})

	



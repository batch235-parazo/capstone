const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})



	
		
	router.post('/currency', (req, res) => {


		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter NAME'
			})
		}

		if(typeof req.body.name != 'string' || req.body.name.length == 0){
			return res.status(400).send({
				'error' : 'Bad Request: cannot read parameter NAME'
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter EX'
			})
		}

		if(typeof req.body.ex != 'object' || Object.keys(req.body.ex).length == 0){
			return res.status(400).send({
				'error' : 'Bad Request: cannot read parameter EX'
			})
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter ALIAS'
			})
		}

		if(typeof req.body.alias != 'string' || req.body.alias.length == 0){
			return res.status(400).send({
				'error' : 'Bad Request: cannot read parameter ALIAS'
			})
		}

		let foundAlias = exchangeRates.find((aliasCheck) => {
			return aliasCheck.alias === req.body.alias
		})

		if(foundAlias){
			return res.status(400).send({
				'error':'Bad Request: ALIAS already exists'
			})	
		}
		if(!foundAlias){
			return res.status(200).send({
			'success': 'No duplicates'
			})
		}

	})






module.exports = router;
